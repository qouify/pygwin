#!/usr/bin/env python3

"""Definition of class Button."""

import typing as tp

from .box import Box
from .label import Label
from .node import Node


class Button(Box):  # pylint: disable=R0904,R0902
    """Button nodes are boxes with a single clickable Node inside it."""

    def __init__(self, node: Node, **kwargs: tp.Any):
        """Initialise a Button node with the Node node packed in it."""
        Box.__init__(self, Label.node_of(node), **kwargs)

    def get_node(self) -> Node:
        """Get the node inside the button."""
        return self.children[0]
