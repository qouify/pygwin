#!/usr/bin/env python3

"""Test module for pygwin.

Provides function go that launches a simple test application.

"""

import sys
import logging
import pygame as pg

from pygwin.all import mdata, StyleClass, WindowSystem, media
from .main import main_panel
from . import glob


def go():
    """Launch the test."""

    #  configure logging
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    stdout_handler = logging.StreamHandler()
    formatter = logging.Formatter('%(levelname)s;%(module)s;%(message)s')
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)
    logger.addHandler(stdout_handler)

    #  pygame initialisation stuff
    pg.init()
    pg.mixer.init()
    pg.font.init()

    screen = pg.display.set_mode((1200, 820))
    pg.display.set_caption(f'pygwin v{mdata.VERSION} - test application')
    StyleClass.load_default()
    media.add_media_path(glob.MEDIA_DIR)
    win_sys = WindowSystem(screen, transparent=False)
    p = main_panel(win_sys)
    p.open(pos=(0, 0))
    pg.key.set_repeat(200, 100)
    clock = pg.time.Clock()
    win_sys.refresh()
    pg.display.update()

    #  default key bindings:
    #    * escape => close window
    #    * tab => move focus forward
    #    * lshift + tab => move focus backward
    #    * enter => activate element with focus
    win_sys.bind_key(pg.K_RETURN, 'activate')
    win_sys.bind_key(pg.K_ESCAPE, 'close-window')
    win_sys.bind_key(pg.K_TAB, 'move-focus-forward')
    win_sys.bind_key(pg.K_TAB, 'move-focus-backward', pressed=[pg.K_LSHIFT])

    #  main loop
    while not win_sys.closed:
        for pgevt in pg.event.get():
            if pgevt.type == pg.QUIT:
                win_sys.closed = True
            win_sys.process_pg_event(pgevt)
        win_sys.refresh(force_redraw=False)
        pg.display.update()
        clock.tick(glob.FPS)

    pg.quit()
    sys.exit(0)


if __name__ == '__main__':
    go()
