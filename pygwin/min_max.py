#!/usr/bin/env python3

"""Definition of class MinMax."""


class _MinMax:  # pylint: disable=too-few-public-methods
    """A _MinMax is any object which is associated a min and a max value.

    Examples are Gauge and Range nodes.

    """

    min_value: int
    max_value: int

    def __init__(self, min_value: int, max_value: int) -> None:
        """Initialise a MinMax object with min_value and max_value."""
        self.min_value = min_value
        self.max_value = max_value
