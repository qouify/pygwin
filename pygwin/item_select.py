#!/usr/bin/env python3

"""Definition of class ItemSelect."""

import typing as tp

from .select import Select


class ItemSelect(Select[tp.Any]):
    """An ItemSelect is a Select with values in a set of dictionary keys."""

    items: tp.List[tp.Any]

    def __init__(
            self,
            items: tp.Dict[tp.Any, str],
            **kwargs: tp.Any):
        """Initialise an ItemSelect node containing items.

        Items must be a dictionary mapping ItemSelect values (which
        can be of any type) to str-able values.  If not None, kwarg
        value (the initial value of the select) must be in items.

        For instance, this creates an ItemSelect nodes with 3 values.
        Initially, s.value == (0, 10):

        s = ItemSelect({
          (0, 10): "child",
          (10, 20): "teenager",
          (21, 120): "grown up"
        })

        """
        self.items = list(items)

        def get_node(item: tp.Any) -> str:
            return items[item]

        def get_prev(item: tp.Any) -> tp.Any:
            return self.items[self.items.index(item) - 1]

        def get_next(item: tp.Any) -> tp.Any:
            return self.items[self.items.index(item) + 1]

        Select.__init__(
            self,
            self.items[0],
            self.items[-1],
            get_prev,
            get_next,
            get_node=get_node,
            **kwargs
        )
