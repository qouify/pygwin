#!/usr/bin/env python3

"""Definition of some helper functions to handle pygame media."""

import os
import logging
import functools
import typing as tp
import pkg_resources
import pygame as pg

from pygwin import util, types
from pygwin.style import DEFAULT


_PATHS = [
    os.path.curdir,
    os.path.join(
        pkg_resources.resource_filename('pygwin', 'data'), 'media'
    )
]

_FONTS: tp.Dict[str, tp.Any] = {
}


@functools.lru_cache(maxsize=10000)
def get(
        mtype: types.media_t,
        file_path: tp.Optional[str],
        **kwargs: tp.Any
) -> tp.Optional[
    tp.Union[pg.surface.Surface, pg.font.Font, pg.mixer.Sound]
]:
    """Load and return a media from the given file path.

    mtype must be in ['image', 'font', 'sound'] or otherwise None is
    returned.  result of the method is cached.  if file_path is a
    relative path, get will look in all paths of the _PATHS
    variable to find it

    (for images) kwarg scale is an optional (int, int): size of the
    resulting image.

    (for images) kwarg rotate if an optional int: rotation angle of
    the resulting image.

    (for fonts) kwarg size is an optional int: size of the resulting
    font.  default size is 24

    """
    if file_path is None:
        return None
    try:
        #  look in _PATHS if file_path is a relative path
        if not os.path.isabs(file_path):
            for mdir in _PATHS:
                abs_path = os.path.join(mdir, file_path)
                if os.path.isfile(abs_path):
                    file_path = abs_path
                    break

        result: tp.Optional[
            tp.Union[pg.surface.Surface, pg.font.Font, pg.mixer.Sound]
        ] = None
        if mtype == 'image':
            result = pg.image.load(file_path).convert_alpha()
            rotate = kwargs.get('rotate')
            if rotate is not None:
                result = pg.transform.rotate(result, rotate)
            scale = kwargs.get('scale')
            if scale is not None:
                result = pg.transform.scale(result, scale)
        elif mtype == 'font':
            result = pg.font.Font(file_path, kwargs.get('size', 24))
        elif mtype == 'sound':
            result = pg.mixer.Sound(file_path)
    except pg.error:
        logging.error('could not load media file "%s"', file_path)
    except FileNotFoundError:
        logging.error('file "%s" does not exist', file_path)
    return result


def add_media_path(path: str, pos: int = 0) -> None:
    """Add directory path to the _PATHS variable used by get.

    The path is added at position pos.

    """
    _PATHS.insert(pos, path)
    get.cache_clear()


def rem_media_path(path: str) -> None:
    """Remove directory path from the _PATHS variable used by get."""
    if path not in _PATHS:
        logging.info('%s is not in PATHS', path)
    else:
        _PATHS.remove(path)
        get.cache_clear()


def get_image(
        img: tp.Union[pg.surface.Surface, str],
        **kwargs: tp.Any
) -> tp.Optional[pg.surface.Surface]:
    """Load and return a pygame Surface.

    Return img if it is already a pygame Surface.  Otherwise load the
    Surface from file path img and return it.  Result of the method is
    cached.  kwargs are the same as for get.

    """
    if isinstance(img, pg.Surface):
        return img
    result = get('image', img, **kwargs)
    if isinstance(result, pg.surface.Surface):
        return result
    return None


def get_image_(  # pylint: disable=invalid-name
        img: tp.Union[pg.surface.Surface, str],
        **kwargs: tp.Any
) -> pg.surface.Surface:
    """Load an image with get_image + check result is not None."""
    result = get_image(img, **kwargs)
    assert result is not None
    return result


def get_font(
        font: str,
        **kwargs: tp.Any
) -> tp.Optional[pg.font.Font]:
    """Load and return a pygame Font.

    Parameter font can be a file path or the id of a font previously
    loaded by load_fonts.  result of the method is cached.
    kwargs are the same as for get.

    """
    if font in _FONTS:
        path = _FONTS[font]['file']
    else:
        path = font
    result = get('font', path, **kwargs)
    if isinstance(result, pg.font.Font):
        return result
    return None


def get_font_(  # pylint: disable=invalid-name
        font: str,
        **kwargs: tp.Any
) -> pg.font.Font:
    """Load a font with get_fonf + check result is not None."""
    result = get_font(font, **kwargs)
    assert result is not None
    return result


def get_sound(
        file_path: str,
        **kwargs: tp.Any
) -> tp.Optional[pg.mixer.Sound]:
    """Load and return a pygame Sound.

    Load the pygame Sound from file file_path and return it.  Result
    of the method is cached.  kwargs are the same as for get.

    """
    result = get('sound', file_path, **kwargs)
    if isinstance(result, pg.mixer.Sound):
        return result
    return None


def play_sound(
        file_path: str,
        wait: bool = False
) -> tp.Optional[pg.mixer.Channel]:
    """Load and play a sound.

    Sound is loaded from file file_path.  If wait is True, play_sound
    waits until sound is finished and returns None.  Otherwise it
    returns the pygame Channel returned by pg.mixer.Sound.play.
    Sounds loaded is cached.

    """
    sound = get_sound(file_path)
    result = None
    if sound is not None:
        logging.info('playing sound %s', file_path)
        result = sound.play()
        if wait:
            wait_sound(result)
            result = None
    return result


def wait_sound(channel: tp.Optional[pg.mixer.Channel]) -> None:
    """Wait as long as the channel is busy.

    The function has no effect if channel is None.

    """
    if channel is not None:
        while channel.get_busy():
            pg.time.wait(10)


def load_fonts(json_file: str) -> None:
    """Load font definitions from file path json_file."""
    data = util.load_json_file(json_file)
    if data is None:
        return
    for font_id, font in data.items():
        _FONTS[font_id] = font
        if font.get('default', False):
            DEFAULT['font'] = font_id
            DEFAULT['font-size'] = font.get('size', 24)


def get_font_size(font_id: str) -> int:
    """Get the size of a font loaded by load_fonts."""
    if font_id in _FONTS:
        if 'size' in _FONTS[font_id]:
            return int(_FONTS[font_id]['size'])
        logging.warning('font %s has an undefined size', font_id)
    else:
        logging.warning('undefined font: %s', font_id)
    return 24
