#!/usr/bin/env python3

"""Definition of class Panel."""

import typing as tp

from . import types
from .window import Window


class Panel(Window):
    """Document this method."""

    def open(self, pos: tp.Optional[types.pos_t] = None) -> None:
        """Open self at the given position."""
        assert pos is not None
        self.win_sys.open_panel(self, pos=pos)
