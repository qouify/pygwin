#!/usr/bin/env python3

"""Definition of class ValuedNode."""

import typing as tp

from .node import Node

T = tp.TypeVar('T')  # pylint: disable=invalid-name


class ValuedNode(tp.Generic[T], Node):
    """A ValuedNode is any node that is associated a value.

    Examples are Checkbox, or InputText nodes.

    """

    def __init__(self, **kwargs: tp.Any):
        """Initialise a ValuedNode initialised with kwarg value."""
        Node.__init__(self, **kwargs)
        self._value: T = tp.cast(T, kwargs.get('value'))

    @property
    def value(self) -> T:
        """Get the current value of the node."""
        return self._value

    def set_value(self, value: T, trigger: bool = True) -> None:
        """Update the value of the node.

        If trigger is True, the on-change event of the node is
        triggered.

        """
        self._value = value
        if trigger:
            self.trigger_on_change()
        self._update_manager()

    def trigger_on_change(self) -> None:
        """Trigger the on-change event of the node."""
        if self.manager is not None:
            self.manager._trigger('on-change', None, self)

            #  we must clear the style cache of the node since the
            #  node may have value dependant styles
            self._clear_style_cache()
