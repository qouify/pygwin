#!/usr/bin/env python3

"""Definition of class Table."""

import typing as tp

from . import types
from .node import Node
from .label import Label


class Table(Node):  # pylint: disable=R0902
    """Table nodes are two-dimensional arrays of nodes."""

    AVAILABLE_STYLES = {
        'hspacing',
        'vspacing'
    }

    def __init__(
            self,
            cells: tp.Optional[tp.Mapping[types.pos_t, Node]] = None,
            colspan: tp.Optional[tp.Mapping[types.pos_t, int]] = None,
            rowspan: tp.Optional[tp.Mapping[types.pos_t, int]] = None,
            **kwargs: tp.Any):
        """Initialise a table Node.

        The table is initially empty if kwarg cells is None. If not
        None, kwarg cells must be an {(int,int): Node} dictionary with
        key=coordinate of the cell and value=content of the cell.
        Cells of which coordinates do not appear in the dictionary are
        empty. If not None, kwarg colspan (resp. rowspan) is an
        {(int,int): int} dict, indicating for each cell the number of
        columns (rows) the cell spans.

        """
        Node.__init__(self, **kwargs)

        self._cells: tp.Dict[types.pos_t, Node] = dict()
        self._rowspan: tp.Dict[types.pos_t, int] = dict()
        self._colspan: tp.Dict[types.pos_t, int] = dict()
        self._cols: int = 0
        self._rows: int = 0
        self._widths: tp.Optional[tp.Dict[int, int]] = None
        self._heights: tp.Optional[tp.Dict[int, int]] = None

        #  cells initialisation
        if cells is None:
            cells = dict()
        if colspan is None:
            colspan = dict()
        if rowspan is None:
            rowspan = dict()
        for ij in cells:
            self.set_cell(
                ij, cells[ij],
                colspan=colspan.get(ij, 1),
                rowspan=rowspan.get(ij, 1)
            )

    def __len__(self) -> int:
        """Get the number of cells in the table."""
        return len(self._cells)

    def new_row(
            self,
            cells: tp.Mapping[int, Node],
            colspan: tp.Optional[tp.Mapping[int, int]] = None,
            rowspan: tp.Optional[tp.Mapping[int, int]] = None
    ) -> None:
        """Add a row composed of cells at the bottom of the table.

        Cells must be a dictionary of {int: Node} with key=column
        number, value=cell content. If not None, colspan and rowspan
        must be dictionaries of {int: int} with key=column number and
        value=colspan or rowspan of the cell.

        """
        i = self._rows
        for j in cells:
            cs = colspan[j] if colspan is not None and j in colspan else 1
            rs = rowspan[j] if rowspan is not None and j in rowspan else 1
            self.set_cell((i, j), cells[j], colspan=cs, rowspan=rs)

    def set_span(
            self,
            ij: types.pos_t,
            colspan: int = 1,
            rowspan: int = 1
    ) -> None:
        """Set the colspan and rowspan of the ij cell of the table.

        Raise ValueError if the cell does not exist.

        """
        if ij not in self._cells:
            raise ValueError(f'{ij} not in cells')
        if colspan > 1:
            self._colspan[ij] = colspan
        if rowspan > 1:
            self._rowspan[ij] = rowspan
        self._reset_size()

    def set_cell(
            self,
            ij: types.pos_t,
            node: Node,
            colspan: int = 1,
            rowspan: int = 1
    ) -> None:
        """Put node in the ij cell of the table.

        The cell is given the specified colspan and rowspan.

        """
        if ij in self._cells:
            self._cells[ij]._unref()
        node = Label.node_of(node)
        self._cells[ij] = node
        self.set_span(ij, colspan=colspan, rowspan=rowspan)
        i, j = ij
        self._rows = max(self._rows, i + self._cell_rows(ij))
        self._cols = max(self._cols, j + self._cell_cols(ij))
        self._add_child(node)
        self._reset_size()

    def empty(self) -> None:
        """Remove all cells from self."""
        children = list(self._cells.values())
        for child in children:
            self._del_child(child)
        self._cols = 0
        self._rows = 0
        self._cells = dict()
        self._rowspan = dict()
        self._colspan = dict()

    def _compute_inner_size(self) -> types.pos_t:
        hspacing = self.get_style('hspacing')
        vspacing = self.get_style('vspacing')

        self._widths = {j: 0 for j in range(self._cols)}
        self._heights = {i: 0 for i in range(self._rows)}

        #  compute sizes of all cells
        s = {
            ij: cell._compute_size()
            for ij, cell in self._cells.items()
        }

        self._compute_column_widths(s)

        #  compute max cell height for each row
        for i in range(self._rows):
            for j in range(self._cols):
                if (i, j) in self._cells:
                    _, min_h = self._cells[i, j].size_
                    self._heights[i] = max(
                        min_h, self._heights[i], s[i, j][1]
                    )

        #  set the container size of all cells
        for i, j in self._cells:
            width = 0
            cols = self._cell_cols((i, j))
            for k in range(cols):
                width += self._widths[j + k]
            width += (cols - 1) * hspacing
            csize = (width, self._heights[i])
            self._cells[i, j]._set_container_size(csize)

        #  compute the final result
        w = 0
        j = 0
        while j < self._cols:
            cols = self._cell_cols((0, j))
            for k in range(cols):
                w += self._widths[j + k]
            w += cols
            j += cols
        h = sum(self._heights[i] for i in range(self._rows))
        if self._cols > 1:
            w += (self._cols - 1) * hspacing
        if self._rows > 1:
            h += (self._rows - 1) * vspacing

        return w, h

    def _position(self, pos: types.pos_t) -> None:
        if self._widths is None or self._heights is None:
            raise ValueError('widths and heights of table not computed')
        hspacing = self.get_style('hspacing')
        vspacing = self.get_style('vspacing')
        h = 0
        i = 0
        while i < self._rows:
            w = 0
            j = 0
            while j < self._cols:
                if (i, j) in self._cells:
                    cell = self._cells[i, j]
                    cell.position((pos[0] + w, pos[1] + h))
                    w += cell.container_size_[0]
                else:
                    w += self._widths[j]
                w += hspacing
                j += self._cell_cols((i, j))
            h += self._heights[i] + vspacing
            i += 1

    def _cell_cols(self, ij: types.pos_t) -> int:
        if ij in self._colspan:
            return self._colspan[ij]
        return 1

    def _cell_rows(self, ij: types.pos_t) -> int:
        if ij in self._rowspan:
            return self._rowspan[ij]
        return 1

    def _compute_column_widths(
            self,
            sizes: tp.Mapping[types.pos_t, types.pos_t]
    ) -> None:
        hspacing = self.get_style('hspacing')
        notdone: tp.Dict[int, tp.Tuple[types.pos_t, int, int]] = dict()
        for j in range(self._cols):
            wmax = 0

            #  traverse cells of this column that spans a single
            #  column
            for i in range(self._rows):
                if (i, j) in self._cells:
                    cell = self._cells[i, j]
                    if self._cell_cols((i, j)) == 1:
                        wmax = max(wmax, cell.size_[0])

            #  check closed cells
            for i, nd in notdone.items():
                _, cw, ccols = nd
                if ccols == 1:
                    wmax = max(cw, wmax)

            #  update opened cells
            for i, nd in notdone.items():
                cij, cw, ccols = nd
                notdone[i] = cij, cw - wmax - hspacing, ccols - 1

            #  remove closing cells
            notdone = {k: v for k, v in notdone.items() if v[2] > 0}

            #  open new cells
            for i in range(self._rows):
                if (i, j) in self._cells:
                    cell = self._cells[i, j]
                    cols = self._cell_cols((i, j))
                    if cols > 1:
                        notdone[i] = \
                            (i, j), sizes[i, j][0] - wmax - hspacing, cols - 1

            assert self._widths is not None
            self._widths[j] = wmax

    def _iter_tree(
            self, rec: bool = True, traverse: bool = False
    ) -> tp.Iterator[Node]:
        if rec:
            for cell in self._cells.values():
                yield from cell.iter_tree(rec=True, traverse=traverse)
        else:
            yield from self._cells.values()

    def _del_child(self, node: Node) -> None:
        super()._del_child(node)
        ij = next(ij for ij, n in self._cells.items() if n == node)
        del self._cells[ij]
        for d in (self._rowspan, self._colspan):
            if ij in d:
                del d[ij]
        self._reset_size()
