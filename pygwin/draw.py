#!/usr/bin/env python3

"""Definition of several drawing helper functions."""

import typing as tp
import pygame as pg

from . import types, pos as pw_pos


BLACK = (0, 0, 0, 0)


def rectangle(
        surface: pg.surface.Surface,
        color: tp.Any,
        rect: tp.Any,
        width: int = 0
) -> None:
    """Draw a rectangle rect."""
    x, y, w, h = rect
    if w == 0 or h == 0:
        return
    if width == 0 or width is None:
        pg.draw.rect(surface, color, rect, width)
    else:
        if width > 2 * w or width > 2 * h:
            msg = f'incorrect width for a ({w}, {h}) rectangle: {width}'
            raise ValueError(msg)
        pg.draw.rect(surface, color, (x, y, w, width))
        pg.draw.rect(surface, color, (x, y, width, h))
        pg.draw.rect(surface, color, (x + w - width, y, width, h))
        pg.draw.rect(surface, color, (x, y + h - width, w, width))


def rectangle_rounded(
        surface: pg.surface.Surface,
        color: tp.Any,
        rect: tp.Any,
        radius: int,
        width: int = 0
) -> None:  # pylint: disable=R0913
    """Draw a rounded rectangle rect."""

    def draw_sub_rects() -> None:
        # draw sub rectangles
        if width == 0 or width is None:
            rects = [
                (x + radius, y, w - 2 * radius + 1, radius),
                (x, y + radius, w, h - 2 * radius),
                (x + radius, y + h - radius, w - 2 * radius + 1, radius)
            ]
        else:
            rects = [
                (x + radius, y, w - 2 * radius + 1, width),
                (x + radius, y + h - width, w - 2 * radius + 1, width),
                (x, y + radius, width, h - 2 * radius),
                (x + w - width, y + radius, width, h - 2 * radius)
            ]
        for r in rects:
            rectangle(surface, color, r)

    def draw_angles() -> None:
        # draw rounded angles
        circ = pg.Surface((radius * 2, radius * 2)).convert_alpha()
        circ.fill(BLACK)
        pg.draw.circle(
            circ, color, (radius, radius), radius
        )
        if not (width == 0 or width is None):
            pg.draw.circle(
                circ, BLACK, (radius, radius), radius - width
            )
        cs = (radius, radius)
        for pos, area in [
                ((x, y), pw_pos.rect((0, 0), cs)),
                ((x + w - radius, y), pw_pos.rect((radius, 0), cs)),
                ((x, y + h - radius), pw_pos.rect((0, radius), cs)),
                ((x + w - radius, y + h - radius), pw_pos.rect(cs, cs))
        ]:
            surface.blit(circ, pos, area=pg.Rect(area))

    x, y, w, h = rect
    radius = min(radius, int(w / 2), int(h / 2))

    # no radius => simple rectangle drawing
    if radius == 0 or radius is None:
        rectangle(surface, color, rect, width)
        return

    draw_sub_rects()
    draw_angles()


def circle(
        surface: pg.surface.Surface,
        color: tp.Any,
        origin: types.pos_t,
        radius: int,
        width: int = 0
) -> None:  # pylint: disable=R0913
    """Draw a circle."""
    if width == 0:
        pg.draw.circle(surface, color, origin, radius)
    else:
        surf = pg.Surface((radius * 2, radius * 2)).convert_alpha()
        surf.fill(BLACK)
        pg.draw.circle(surf, color, (radius, radius), radius)
        pg.draw.circle(surf, BLACK, (radius, radius), radius - width)
        surface.blit(surf, (origin[0] - radius, origin[1] - radius))
