#!/usr/bin/env python3

"""Definition of class Animation."""

import itertools
import typing as tp

from . import types
if tp.TYPE_CHECKING:
    from .window import Window  # pylint: disable=unused-import


class Animation:
    #  pylint: disable=too-many-instance-attributes
    """An Animation is some function that is executed periodically.

    An animation has a period (expressed in the game main loop
    iterations) ; and an handler which is a function taking as
    argument some progression value and returning an updated progress
    value.  The animation keeps executing as long as the progress
    value returned by the handler function is not None.

    """

    callback: tp.Optional[types.animation_callback_t]
    paused: bool
    period: int
    prog: tp.Any
    started: bool
    stopped: bool

    _ALL: tp.Dict['Window', tp.List['Animation']] = dict()

    def __init__(
            self,
            prog: tp.Any,
            handler: tp.Callable[[tp.Any], tp.Any],
            win: 'Window',
            period: int = 1
    ):
        """Initialise an Animation with the given handler.

        prog is the initial progression value of the animation.  The
        animation is attached to Window win.  This means that the
        animation is stopped when win is closed.

        """
        self.callback = None
        self.paused = False
        self.period = period
        self.prog = prog
        self.started = False
        self.stopped = False

        self._handler: tp.Callable[[tp.Any], tp.Any] = handler
        self._last: int = 0
        self._win: 'Window' = win

    def pause(self) -> None:
        """Pause self."""
        self.paused = True

    def start(self, start_now: bool = False) -> None:
        """Start self.

        If start_now is True, the animation handler is executed right
        now.  Otherwise it first occurs in self.period iterations.

        """
        self.paused = False
        if self.started:
            return
        self.started = True
        if start_now:
            self.prog = self._handler(self.prog)
        if self.prog is None:
            self.stop()
        else:
            Animation._ALL.setdefault(self._win, [])
            Animation._ALL[self._win].append(self)

    def has_terminated(self) -> bool:
        """Check if self has terminated."""
        return self.prog is None

    def check_run(self) -> bool:
        """Check if self must be runned now.

        Return True if the handler function must be called now, False
        otherwise.  The handler must be called if (1) the animation
        has been started (2) not paused and (3) more than self.period
        iterations has elapsed since the last time it has been runned.

        """
        self._last += 1
        return (
            not self.paused
            and self.prog is not None
            and self._last >= self.period
        )

    def run(self) -> None:
        """Run self now.

        The handler of self is called and its progression value is
        updated.

        """
        self.prog = self._handler(self.prog)
        self._last = 0

    @classmethod
    def run_all(cls, wins: tp.Optional[tp.List['Window']] = None) -> bool:
        """Run all animations that must be run now.

        Returns True if at least one animation has been runned, False
        otherwise.  If wins is not None, then only the animations of
        Windows in wins must be run.

        """
        result = False
        if wins is None:
            wins = list(Animation._ALL.keys())
        for win in wins:
            done = []
            for anim in Animation._ALL.get(win, []):
                if anim.check_run():
                    anim.run()
                    result = True
                if anim.has_terminated():
                    done.append(anim)
                    anim.stop()
            for anim in done:
                #  the animation execution or callback may have closed
                #  the window.  hence we must check it still in _ALL
                if win in Animation._ALL:
                    Animation._ALL[win].remove(anim)
                    if Animation._ALL[win] == []:
                        del Animation._ALL[win]
        return result

    def stop(self) -> None:
        """Stop self.

        Self's callback is called.

        """
        self.prog = None
        stopped = self.stopped
        self.stopped = True
        if not stopped and self.callback is not None:
            self.callback()  # pylint: disable=not-callable

    @classmethod
    def stop_all(cls, wins: tp.Optional[tp.List['Window']] = None) -> None:
        """Stop all animations.

        If wins is not None, then only the animations of Windows in
        wins are stopped.

        """
        if wins is None:
            stopped = itertools.chain(*Animation._ALL.values())
            new_all = dict()
        else:
            stopped = itertools.chain(
                [val for win in wins for val in Animation._ALL.get(win, [])]
            )
            new_all = {
                win: anims
                for win, anims in Animation._ALL.items()
                if win not in wins
            }
        Animation._ALL = new_all
        for anim in stopped:
            anim.stop()
