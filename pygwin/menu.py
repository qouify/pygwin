#!/usr/bin/env python3

"""Definition of class Menu."""

import typing as tp
import pygame as pg

from . import types, util
from .rule import Rule
from .horizontal_rule import HorizontalRule
from .vertical_rule import VerticalRule
from .box import Box
from .empty import Empty
from .node import Node


class Menu(Box):
    """Menu nodes are collection of item nodes.

    Each item node is associated with one header node.  Only one item
    is visible at any time.  Activation of one of the headers shows
    its associated item and hides the others.

    """

    items: tp.Dict[Node, Node]

    def __init__(self, items: tp.Dict[Node, Node], **kwargs: tp.Any):
        """Initialise a Menu node.

        Items is a dictionary mapping header nodes to children nodes.

        """
        def on_mouse_wheel(pgevt: pg.event.Event) -> bool:
            if pgevt.button is util.MOUSEBUTTON_WHEEL_DOWN:
                self._prev_tab()
            elif pgevt.button is util.MOUSEBUTTON_WHEEL_UP:
                self._next_tab()
            return True

        def switch_tab(i: int) -> types.link_t:
            def result(i: int) -> bool:
                self.switch_tab(i)
                return True
            return lambda: result(i)

        Box.__init__(self, **kwargs)
        self.items = dict()
        self._curr: tp.Optional[Node] = None

        for i, item in enumerate(items):
            item.set_link(switch_tab(i))
            self.items[item] = items[item]
        rule: Rule
        if self.get_style('orientation') == 'vertical':
            rule = HorizontalRule()
            orientation = 'horizontal'
        else:
            rule = VerticalRule()
            orientation = 'vertical'
        style = {
            'orientation': orientation,
            'halign': 'center',
            'valign': 'center'
        }
        head_box = Box(style=style)
        for item in self.items:
            head_box.pack(item)
        self.pack(head_box)
        self.pack(rule)
        self.pack(Empty())

        head_box.add_processor('on-mouse-wheel', on_mouse_wheel)
        self.switch_tab(0)

    def switch_tab(self, i: int) -> None:
        """Switch the visible item node to the ith one."""
        if self._curr is not None:
            self._curr.set_selected(False)
            if self.manager is not None:
                self.manager._trigger('on-unselect', None, self._curr)
        self._curr = self._item_at_index(i)
        self._curr.set_selected(True)
        node = self.items[self._curr]
        self.replace(2, node)
        if self.manager is not None:
            self.manager._trigger('on-select', None, self._curr)
        self._update_manager()

    def _item_at_index(self, idx: int) -> Node:
        for i, item in enumerate(self.items):
            if i == idx:
                return item
        raise IndexError('item not found')

    def _item_index(self, item: Node) -> int:
        for i, it in enumerate(self.items):
            if it == item:
                return i
        raise IndexError('item not found')

    def _next_tab(self) -> None:
        idx = 0 if self._curr is None else (
            (self._item_index(self._curr) + 1) % len(self.items)
        )
        self.switch_tab(idx)

    def _prev_tab(self) -> None:
        idx = 0 if self._curr is None else (
            (self._item_index(self._curr) - 1) % len(self.items)
        )
        self.switch_tab(idx)
