#!/usr/bin/env python3

"""Definition of class _EventManager."""

import abc
import typing as tp
import pygame as pg

from . import types, util, pos as pw_pos
from .node import Node


class _EventManager(Node):  # pylint: disable=R0904,R0902
    """An _EventManager is some node that can catch and process elements.

    Frame and Window nodes are examples of _EventManager nodes.

    Each _EventManager is associated with a surface on which it is
    drawn.

    _EventManager objects are hierarchically organised.  Each can have
    children and a parent.  An _EventManager that does not have a
    parent is called a root _EventManager.  It usually is a Window
    node.

    """

    surface: tp.Optional[pg.surface.Surface]

    def __init__(self, **kwargs: tp.Any) -> None:
        """Initialise self."""
        Node.__init__(self, **kwargs)
        self.surface = None

        self._updated: bool = True
        self._children: tp.List[_EventManager] = list()
        self._overed: tp.Set[Node] = set()
        self._clicked: tp.Set[Node] = set()
        self._floating: tp.Set[Node] = set()
        self._registered: tp.Dict[
            types.event_t, tp.List[tp.Tuple[Node, types.event_proc_t]]
        ] = {
            evt: list() for evt in types.all_events
        }

    @property
    def surface_(self) -> pg.surface.Surface:
        """Get the surface of self (+ check it is not None)."""
        assert self.surface is not None
        return self.surface

    def make_visible(self, node: Node) -> None:
        """Update self's scrolling so that node becomes fully visible."""

    def _get_scroll(self) -> types.pos_t:
        return 0, 0

    def _add_child_manager(self, child: '_EventManager') -> None:
        if child not in self._children and child != self:
            self._children.append(child)

    def _del_child_manager(self, child: '_EventManager') -> None:
        self._children.remove(child)

    def _set_manager(self, manager: tp.Optional['_EventManager']) -> None:
        if manager is not None:
            manager._add_child_manager(self)
        super()._set_manager(manager)

    def is_root(self) -> bool:
        """Check if self is a root _EventManager (with no parent manager)."""
        return self.manager is None or self.manager == self

    def _update_manager(self) -> None:
        if not self.is_root():
            self.manager_._update_manager()
        super()._update_manager()

    def _reset_manager(self) -> None:
        if not self.is_root():
            self.manager_._del_child_manager(self)
        super()._reset_manager()

    def _set_updated(self, updated: bool) -> None:
        self._updated = updated

    def _recompute_sizes_and_positions(self) -> None:
        content = self.get_content()

        #  position main node
        content._compute_size()
        content.position((0, 0))

        #  and floating nodes
        size = content.size_
        for node in self._floating:
            nsize = node._compute_size()
            pos_style = node.get_style('pos')
            if pos_style is None:
                pos = 0, 0
            else:
                pos = pw_pos.floating_to_pos(pos_style, nsize, size)
            node.position(pos)

        for child_manager in self._children:
            child_manager._recompute_sizes_and_positions()

    def _draw_content(self) -> None:
        if self._updated:

            self._recompute_sizes_and_positions()

            #  reinitialise the surface if needed
            size = self.get_content().size_
            if self.surface is None or self.surface.get_size() != size:
                self.surface = pg.Surface(size).convert_alpha()

            self.surface_.fill((0, 0, 0, 0))
            self._set_updated(False)

            #  draw all children nodes
            for node in self.iter_nodes(rec=False):
                # if node != self:
                #     node._set_manager(self)
                node.draw(self.surface_)

    def add_floating_node(self, node: Node) -> None:
        """Add a floating node to self.

        Node is positionned with style pos.  If style pos is not
        defined, node is positionned at (0, 0) in self.

        """
        node._set_manager(self)
        self._floating.add(node)
        for child in node.iter_tree():
            child.depth = 0
        self._set_updated(True)

    def del_floating_node(self, node: Node) -> None:
        """Remove floating node node from self."""
        if node in self._floating:
            node._unref()
            self._floating.remove(node)
        self._set_updated(True)

    def _register(
            self,
            evt: types.event_t,
            proc: Node,
            fun: types.event_proc_t
    ) -> None:
        self._registered[evt].append((proc, fun))

    def _unregister(
            self,
            evt: types.event_t,
            proc: Node,
            fun: types.event_proc_t
    ) -> None:
        self._registered[evt] = [
            pf
            for pf in self._registered[evt]
            if pf != (proc, fun)
        ]

    def _unref_node(self, node: Node) -> None:
        win = self.get_window()
        if win != self:
            win._unref_node(node)
        for evt in self._registered:
            self._registered[evt] = [
                pf
                for pf in self._registered[evt]
                if pf[0] != node
            ]

    def _trigger(
            self,
            evt: types.event_t,
            pgevt: tp.Optional[pg.event.Event],
            proc: Node
    ) -> bool:
        return self._event(evt, pgevt, check_pos=False, incl={proc})

    def _check_pos_over(self, pos: types.pos_t, node: Node) -> bool:
        return node.is_over(pos) and all(
            f.depth >= node.depth
            or not f.is_over(pos)
            for f in self._floating
        )

    def _event(
            self,
            evt: types.event_t,
            pgevt: tp.Optional[pg.event.Event],
            **kwargs: tp.Any
    ) -> bool:
        result = False
        check_pos = kwargs.get('check_pos', True)
        incl = kwargs.get('incl', None)
        if pgevt is None:
            pgevt = pg.event.Event(0)

        for proc, fun in self._registered[evt]:

            #  it may be the case that the manager of proc is not self
            #  anymore if a processor in self._registered[evt]
            #  removed it (call to fun in this loop).  if so we skip
            #  it
            # if proc.manager != self:
            #    continue

            #  the event has a position and this one is not inside the
            #  node => skip it
            if (
                    check_pos
                    and pgevt is not None
                    and hasattr(pgevt, 'pos')
                    and not self._check_pos_over(pgevt.pos, proc)
            ):
                continue

            #  the node is not in included => skip it
            if incl is not None and proc not in incl:
                continue

            #  the node is hidden => skip it
            if proc.is_hidden():
                continue

            if fun(pgevt):
                result = True

        return result

    def _pg_mouse_motion(self, pgevt: pg.event.Event) -> bool:
        result = False

        #  check which nodes are not overed anymore and which are
        #  newly overed.  set the overed flag of these and trigger the
        #  on-clicked and on-unclicked events if needed
        not_overed_anymore = {
            n for n in self._overed
            if not self._check_pos_over(pgevt.pos, n)
        }
        newly_overed = {
            n for n in self.iter_nodes()
            if self._check_pos_over(pgevt.pos, n)
            and n not in self._overed
        }
        self._overed = self._overed - not_overed_anymore
        for n in not_overed_anymore:
            n._set_overed(False)
            if n.is_clicked():
                result = self._trigger('on-unclicked', pgevt, n) or result
        for n in newly_overed:
            n._set_overed(True)
            if n.is_clicked():
                result = self._trigger('on-clicked', pgevt, n) or result

        #  trigger events
        events: tp.List[tp.Tuple[types.event_t, tp.Set[Node]]] = [
            ('on-unover', not_overed_anymore),
            ('on-over-again', self._overed),
            ('on-over', newly_overed)
        ]
        for evt, node_set in events:
            result = self._event(
                evt, pgevt, check_pos=False, incl=node_set
            ) or result

        self._overed |= set(newly_overed)

        return result

    def _pg_mouse_button_up(self, pgevt: pg.event.Event) -> bool:
        if pgevt.button == util.MOUSEBUTTON_LEFT:
            result = self._event('on-click-up', pgevt)
            result = self._event(
                'on-unclicked', pgevt, check_pos=False, incl=self._clicked
            ) or result
            for n in self._clicked:
                n._set_clicked(False)
            self._clicked = set()
            return result
        if pgevt.button == util.MOUSEBUTTON_RIGHT:
            return self._event('on-click-up-right', pgevt)
        if pgevt.button in [
                util.MOUSEBUTTON_WHEEL_DOWN,
                util.MOUSEBUTTON_WHEEL_UP
        ]:
            return self._event('on-mouse-wheel', pgevt)
        return False

    def _pg_mouse_button_down(self, pgevt: pg.event.Event) -> bool:
        evt: tp.Optional[types.event_t] = None
        result = False
        if pgevt.button == util.MOUSEBUTTON_LEFT:
            evt = 'on-click-down'
            self._clicked = {
                n for n in self.iter_nodes()
                if self._check_pos_over(pgevt.pos, n)
            }
            result = self._event(
                'on-clicked', pgevt, check_pos=False, incl=self._clicked
            ) or result
            for n in self._clicked:
                n._set_clicked(True)
        elif pgevt.button == util.MOUSEBUTTON_RIGHT:
            evt = 'on-click-down-right'
        else:
            return False

        result = self._event(evt, pgevt) or result

        return result

    def _get_cursor_image(self) -> tp.Optional[tp.Any]:
        for node in self.iter_tree(traverse=True):
            result = node.get_style('cursor-image')
            if result is not None:
                return result
        return None

    def _pg_key_down(self, pgevt: pg.event.Event) -> bool:
        return self._event('on-key', pgevt)

    def process_pg_event(self, pgevt: pg.event.Event) -> bool:
        """Process pygame event pgevt.

        Return True if the event has been processed by some node
        inside self, False otherwise.

        """
        try:
            proc: tp.Optional[types.event_proc_t] = {
                pg.MOUSEBUTTONDOWN: self._pg_mouse_button_down,
                pg.MOUSEBUTTONUP: self._pg_mouse_button_up,
                pg.MOUSEMOTION: self._pg_mouse_motion,
                pg.KEYDOWN: self._pg_key_down
            }[pgevt.type]
        except KeyError:
            result = False
            proc = None

        if proc is not None:
            result = proc(pgevt)

        for child_manager in self._children:
            if not hasattr(pgevt, 'pos') or child_manager.is_over(pgevt.pos):
                result = child_manager.process_pg_event(pgevt) or result

        if result:
            self._set_updated(True)

            #  since sizes and positions may have changed we recompute
            #  them
            if self.is_root():
                self._recompute_sizes_and_positions()

        return result

    def iter_nodes(self, rec: bool = True) -> tp.Iterator[Node]:
        """Iterate on all the nodes of the manager."""
        if not rec:
            yield self.get_content()
            yield from self._floating
        else:
            yield from self.get_content().iter_tree()
            for floating in self._floating:
                for node in floating.iter_tree():
                    yield node

    def _clear(self) -> bool:
        result = self._event(
            'on-unover', None, check_pos=False, incl=self._overed
        )
        for n in self._clicked:
            result = n._set_clicked(False) or result
        for n in self._overed:
            result = n._set_overed(False) or result
        self._clicked = set()
        self._overed = set()
        return result

    @abc.abstractmethod
    def get_content(self) -> Node:
        """Get the node inside self."""

    @abc.abstractmethod
    def available_size(self) -> types.opt_pos_opt_t:
        """Get the total available size in self."""
