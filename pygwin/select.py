#!/usr/bin/env python3

"""Definition of class Select."""

import typing as tp

from . import types, util
from .box import Box
from .label import Label
from .valued_node import ValuedNode
from .node import Node
from .empty import Empty

T = tp.TypeVar('T')  # pylint: disable=invalid-name


class Select(Box, tp.Generic[T], ValuedNode[T]):  # pylint: disable=R0902
    """Select nodes are comparable to HTML <select> elements.

    Select nodes contain a fixed list of items the user can navigate
    through.  a select consists of a node and two links allowing to
    navigate backward or forward in the list.  unlike select elements,
    it is only possible to navigate sequentially through the list.

    Note that the IntSelect and ItemSelect classes inheriting from Select
    should be prefered for most cases.

    """

    AVAILABLE_STYLES = {
        'select-cyclic',
        'select-hide-links',
        'select-next-class',
        'select-next-label',
        'select-prev-class',
        'select-prev-label',
        'select-wheel-units'
    }

    NEXT: int = 1
    PREV: int = -1

    def __init__(
            self,
            first: tp.Any,
            last: tp.Any,
            get_prev: tp.Callable[[tp.Any], tp.Any],
            get_next: tp.Callable[[tp.Any], tp.Any],
            **kwargs: tp.Any
    ) -> None:
        """Initialize a Select node ranging from first to last.

        get_prev and get_next are mapping from a select value to its
        predecessor or successor.

        kwarg value is the initial value of the select.  default is
        first.

        kwarg get_node is a function from select values to anything
        that can str-ed.

        if not None, kwarg prev_node (resp. next_node) is the node
        that will be the navigation link to move to the previous
        (resp. next) value of the select.

        for instance:
        Select(0, 100, lambda n: n - 1, lambda n: n + 1, value=50)
        creates a list with values ranging from 0 to 100 and
        initialised to 50.  (note that for int Select, IntSelect
        should be used instead)

        """
        def create_navigation_links() -> tp.Iterator[Node]:
            def get_link(name: str) -> Node:
                c = self.get_style(
                    tp.cast(types.style_attr_t, f'select-{name}-class')
                )
                lbl = self.get_style(
                    tp.cast(types.style_attr_t, f'select-{name}-label')
                )
                if lbl != '' and lbl is not None:
                    return Label(lbl, stc=c)
                return Empty(stc=c)
            for name in ['prev', 'next']:
                node: tp.Optional[Node] = kwargs.get(name + '_node')
                if node is None:
                    node = get_link(name)
                yield node
        value = kwargs.get('value', first)
        kwargs['value'] = value
        Box.__init__(self, **kwargs)
        ValuedNode.__init__(self, **kwargs)
        self._first: tp.Any = first
        self._last: tp.Any = last
        self._get_prev: tp.Callable[[tp.Any], tp.Any] = get_prev
        self._get_next: tp.Callable[[tp.Any], tp.Any] = get_next
        self._get_node: tp.Callable[[tp.Any], tp.Any] = kwargs.get(
            'get_node', str
        )
        self._lbl: Label = Label(str(self._get_node(value)))
        lbl_box = Box(self._lbl)
        nav_nodes = tuple(create_navigation_links())
        nav_nodes[0].set_link(lambda: self._move(Select.PREV))
        nav_nodes[1].set_link(lambda: self._move(Select.NEXT))
        self._links: tp.Dict[int, Node] = {
            Select.PREV: nav_nodes[0],
            Select.NEXT: nav_nodes[1]
        }
        self.pack(self._links[Select.PREV])
        self.pack(lbl_box)
        self.pack(self._links[Select.NEXT])
        self.set_value(value, trigger=False)
        self.add_processor('on-mouse-wheel', self._mouse_wheel_event)

    def set_value(self, value: tp.Any, trigger: bool = True) -> None:
        """Set value of the select to value.

        If trigger is True, the ON_CHANGE event of the select is
        triggered.

        """
        ValuedNode.set_value(self, value, trigger=trigger)
        self._lbl.text = str(self._get_node(value))
        if not self._is_cyclic():
            if value != self._first:
                self.enable_move(Select.PREV)
            if value != self._last:
                self.enable_move(Select.NEXT)
            if value == self._first:
                self.disable_move(Select.PREV)
            if value == self._last:
                self.disable_move(Select.NEXT)

    def disable_move(self, move: int) -> None:
        """Disable a navigation link of the select.

        Raise ValueError if move does not belong to {Select.NEXT,
        Select.PREV}.  If the disabled link had the focus, give focus
        to the other navigation link (if it's enabled).

        """
        if move not in [Select.NEXT, Select.PREV]:
            raise ValueError(f'move must be in {[Select.NEXT, Select.PREV]}')
        has_focus = self._links[move].has_focus()
        if self._hide_links():
            self._links[move].set_hidden(True)
        self._links[move].disable()
        if has_focus and self._links[-move].can_grab_focus_now():
            self._links[-move].get_focus()

    def enable_move(self, move: int) -> None:
        """Enable a navigation link of the select.

        Raise ValueError if move does not belong to {Select.NEXT,
        Select.PREV}.

        """
        if move not in [Select.NEXT, Select.PREV]:
            raise ValueError(f'move must be in {[Select.NEXT, Select.PREV]}')
        if self._hide_links():
            self._links[move].set_hidden(False)
        self._links[move].enable()

    def reset(self, first: tp.Any, last: tp.Any, value: tp.Any) -> None:
        """Set the first, last and current value of the select."""
        self._first = first
        self._last = last
        self.set_value(value)

    def _is_cyclic(self) -> bool:
        return bool(self.get_style('select-cyclic'))

    def _hide_links(self) -> bool:
        return bool(self.get_style('select-hide-links'))

    def _move(self, move: int) -> bool:
        if self._links[move].is_disabled():
            return False
        if move == Select.PREV:
            if self.value == self._first:
                new = self._last if self._is_cyclic() else None
            else:
                new = self._get_prev(self.value)
        else:
            if self.value == self._last:
                new = self._first if self._is_cyclic() else None
            else:
                new = self._get_next(self.value)
        if new is not None:
            self.set_value(new)
        return new is not None

    def _mouse_wheel_event(self, pgevt: tp.Any) -> bool:
        if pgevt.button == util.MOUSEBUTTON_WHEEL_DOWN:
            move = Select.NEXT
        elif pgevt.button == util.MOUSEBUTTON_WHEEL_UP:
            move = Select.PREV
        if move is not None:
            for _ in range(self.get_style('select-wheel-units')):
                self._move(move)
        return True
