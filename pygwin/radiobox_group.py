#!/usr/bin/env python3

"""Definition of class RadioboxGroup."""

import typing as tp

from .radiobox import Radiobox


class RadioboxGroup:
    """A RadioboxGroup groups Radiobox nodes that are mutually exclusive.

    If a Radiobox is selected among the group, then others become
    deselected.  RadioboxGroups are not visible on the screen, they
    are just logical groups of Radiobox nodes.

    """

    def __init__(self) -> None:
        """Initialise a group with an empty set of radioboxes."""
        self._selected: tp.Optional[Radiobox] = None
        self._values: tp.Dict[Radiobox, tp.Any] = dict()

    def get_value(self) -> tp.Any:
        """Get the value associated to the currently checked box of the group.

        Returns None if no box is currently checked.

        """
        if self._selected is None:
            result = None
        else:
            result = self._values[self._selected]
        return result

    def add_radiobox(self, box: Radiobox, value: tp.Any) -> None:
        """Add Radiobox box to the group and associate the given value."""
        self._values[box] = value

    def select(self, box: Radiobox) -> None:
        """Radiobox box becomes the selected Radiobox of the group.

        Raises ValueError is raised if the box does not belong to the
        group.

        """
        if box not in self._values:
            raise ValueError('box does not belong to the group values')
        if box != self._selected:
            if self._selected is not None:
                self._selected.set_value(False)
            self._selected = box
            self._selected.set_value(True)
