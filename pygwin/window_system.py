#!/usr/bin/env python3

"""Definition of class WindowSystem."""

import time
import logging
import typing as tp
import pygame as pg

from . import types, cursor, media
from .animation import Animation
if tp.TYPE_CHECKING:
    from .window import Window  # pylint: disable=unused-import
    from .panel import Panel  # pylint: disable=unused-import


class WindowSystem:  # pylint: disable=R0902
    """WindowSystem is pygwin's main class."""

    closed: bool
    frozen: bool
    screen: pg.surface.Surface

    def __init__(self, screen: pg.surface.Surface, transparent: bool = True):
        """Initialize a window system in the given screen surface.

        >>> import pygame as pg
        >>> from pygwin.all import WindowSystem
        >>> _ = pg.init()
        >>> screen = pg.display.set_mode((800, 600))
        >>> win_sys = WindowSystem(screen)
        >>> win_sys.refresh()

        """
        self.closed = False
        self.frozen = False
        self.screen = screen

        if transparent:
            self._surface = pg.Surface(screen.get_size()).convert_alpha()
        else:
            self._surface = pg.Surface(screen.get_size())
        self._windows: tp.List['Window'] = list()
        self._panels: tp.Set['Panel'] = set()
        self._redraw_all = True
        self._redraw_cursor = True
        self._mouse = True
        self._last_redraw: tp.Optional[float] = None
        self._key_map: tp.Dict[
            int,
            tp.Dict[
                tp.Sequence[int],
                tp.Tuple[
                    types.key_action_t, tp.Optional[types.user_key_proc_t]
                ]
            ]
        ] = dict()

    def top_window(self) -> tp.Optional['Window']:
        """Get the top window of self (i.e., the last opened window).

        Return None if no window is currently opened.

        """
        if self._windows == []:
            result = None
        else:
            result = self._windows[0]
        return result

    def center_window(self, win: 'Window') -> None:
        """Center window object win in self."""
        sw, sh = self._surface.get_size()
        win._compute_size()
        w, h = win.size_
        win.absolute_pos = (int((sw - w) / 2), int((sh - h) / 2))

    def open_window(
            self,
            win: 'Window',
            pos: types.opt_pos_t = None
    ) -> None:
        """Open Window win in self.

        Win is position at position pos if not None or otherwise centered.

        """
        self._windows.insert(0, win)
        if pos is None:
            self.center_window(win)
        else:
            win.absolute_pos = pos

    def window_opened(self, win: 'Window') -> bool:
        """Check if window win has been opened in self."""
        return win in self._windows or win in self._panels

    def close_window(self, win: tp.Optional['Window']) -> None:
        """Close window win of self.

        If win is None, the top window is closed.

        """
        if win is None:
            if self._windows == []:
                return
            win = self._windows[0]
        if win in self._windows:
            self._windows.remove(win)
            Animation.stop_all(wins=[win])

    def close_all_windows(self) -> None:
        """Close all the windows of self."""
        while self._windows != []:
            self.close_window(self._windows[0])

    def center_all_windows(self) -> None:
        """Center all windows of self."""
        for win in self._windows:
            self.center_window(win)

    def open_panel(self, panel: 'Panel', pos: types.pos_t) -> None:
        """Open Panel panel in self at position pos."""
        self._panels.add(panel)
        panel.absolute_pos = pos

    def close_panel(self, panel: 'Panel') -> None:
        """Close Panel panel of self."""
        if panel in self._panels:
            self._panels.remove(panel)

    def process_pg_event(self, *pgevts: pg.event.Event) -> bool:
        """Process pygame events pgevts.

        Return True if some event has been processed, False otherwise.

        """
        #  self is frozen => events are ignored
        if self.frozen:
            return False

        result = False
        mouse_moved = False
        for pgevt in pgevts:

            #  if mouse is disabled, several event types are ignored
            if not self._mouse and pgevt.type in [
                    pg.MOUSEBUTTONDOWN,
                    pg.MOUSEBUTTONUP,
                    pg.MOUSEMOTION,
                    pg.MOUSEWHEEL
            ]:
                continue

            mouse_moved = mouse_moved or pgevt.type in [
                pg.MOUSEBUTTONDOWN, pg.MOUSEBUTTONUP, pg.MOUSEMOTION
            ]

            #  if it's a key event check if some action has been bound
            #  to it
            if pgevt.type == pg.KEYDOWN:
                if self._get_action_key(pgevt):
                    result = True
                    continue

            #  check if some window or panel processes the event.
            #  stop at the first model window
            for win in self._windows + list(self._panels):
                if win.process_pg_event(pgevt):
                    result = True
                if win.modal:
                    break

        self._redraw_all = self._redraw_all or result
        self._redraw_cursor = (
            self._redraw_cursor or cursor.activated() and mouse_moved
        )
        return result

    def draw(self) -> None:
        """Draw self (i.e., its windows and panels) on self.screen."""
        #  if we have not redrawn for 1 second => force redraw
        now = time.time()
        if not self._redraw_all:
            if self._last_redraw is None:
                self._redraw_all = True
            elif now - self._last_redraw >= 1:
                self._redraw_all = True

        if self._redraw_all:
            self._last_redraw = now
            self._surface.fill((0, 0, 0, 0))
            for panel in list(self._panels):
                panel.blit(self._surface)
            for win in self._windows[::-1]:  # last window opened first
                win.blit(self._surface)
            self.screen.blit(self._surface, (0, 0))
            self._draw_cursor()
        elif self._redraw_cursor:
            self.screen.blit(self._surface, (0, 0))
            self._draw_cursor()
        self._redraw_all = False
        self._redraw_cursor = False

    def _active(self) -> tp.List['Window']:
        if self._windows != []:
            result = [self._windows[0]]
        else:
            result = list(self._panels)
        return result

    def refresh(self, force_redraw: bool = True) -> None:
        """Redraw self.

        Run all animations of the top window or of all panels if no
        window is opened.

        The window system is then redrawned if something has been
        updated in it (e.g., the size of one window) or if
        force_redraw is True.

        """
        self._redraw_all = (
            Animation.run_all(wins=self._active())
            or self._redraw_all
            or force_redraw
        )
        self.draw()

    def _draw_cursor(self) -> None:
        if not cursor.activated():
            return

        #  check if some node overrides the cursor image
        for win in self._active():
            img = win._get_cursor_image()
            if img is not None:
                img = media.get_image(img)
                if img is not None:
                    self.screen.blit(img, pg.mouse.get_pos())
                    return

        #  otherwise get the default cursor image
        img = None
        if pg.mouse.get_pressed(num_buttons=3)[0]:
            img = cursor.get_default(status='clicked')
        if img is None:
            img = cursor.get_default()
        if img is not None:
            self.screen.blit(img, pg.mouse.get_pos())

    def disable_mouse(self) -> None:
        """Totally disable the mouse for self.

        After the call, no cursor appears at all and mouse related
        events are not processed anymore.

        """
        self._mouse = False
        cursor.deactivate()
        pg.mouse.set_visible(False)

    def enable_mouse(self) -> None:
        """Enable the mouse for self.

        Mouse cursor becomes visible and mouse related events are
        processed again.

        """
        self._mouse = True
        pg.mouse.set_visible(True)

    def _get_action_key(self, pgevt: pg.event.Event) -> bool:

        #  check in __key_map which action to perform
        acts = self._key_map.get(pgevt.key)
        if acts is None:
            return False
        best: tp.Optional[tp.Sequence[int]] = None
        for keys in acts:
            if (
                    all(pg.key.get_pressed()[key] for key in keys) and
                    (best is None or len(best) < len(keys))
            ):
                best = keys
        if best is None:
            return False
        bind = acts[best]

        def close_window() -> bool:
            win = self.top_window()
            if win is not None:
                win.close()
            return True

        def activate_focus() -> bool:
            return any(w.activate_focus() for w in self._active())

        def move_focus_forward() -> bool:
            return any(w.move_focus_sequential(True) for w in self._active())

        def move_focus_backward() -> bool:
            return any(w.move_focus_sequential(False) for w in self._active())

        def move_focus_direction(direct: tp.Tuple[int, int]) -> types.link_t:
            def fun() -> bool:
                return any(
                    w.move_focus_direction(direct) for w in self._active()
                )
            return fun

        def user_defined(
                fun: tp.Optional[types.user_key_proc_t]
        ) -> types.link_t:
            def do() -> bool:
                if fun is not None:
                    fun()
                return True
            return do
        act, ufun = bind
        try:
            fun = {
                'activate': activate_focus,
                'close-window': close_window,
                'move-focus-forward': move_focus_forward,
                'move-focus-backward': move_focus_backward,
                'move-focus-north': move_focus_direction((0, -1)),
                'move-focus-east': move_focus_direction((1, 0)),
                'move-focus-south': move_focus_direction((0, 1)),
                'move-focus-west': move_focus_direction((-1, 0)),
                'user-defined': user_defined(ufun)
            }[act]
        except KeyError:
            logging.error('undefined action: "%s"', act)
            return False
        return fun()

    def bind_key(
            self,
            key: int,
            action: tp.Optional[types.key_action_t],
            pressed: tp.Optional[tp.Sequence[int]] = None,
            fun: tp.Optional[types.user_key_proc_t] = None
    ) -> None:
        """Associate an action to a key.

        For example, we can associate the escape key to the action of
        closing the top window of the window system.  A number of
        predefined action are available: closing the top window,
        moving the focus...  (see types.key_action_t).  It is also
        possible to define new action as functions to call when
        specific keys are pressed (the 'user-defined' action).

        The method has no effect if action is not in
        types.all_keys_actions.  If not None, pressed is the list of
        pygame keys that must be already pressed when key is pressed
        for the action to be triggered.  If action == 'user-defined',
        fun must not be None and is the function associated to the
        action.

        For instance this associates the RETURN key to the action of
        activating the node that now has the focus ; the F1 key to the
        action of calling method print_help ; and Ctrl+C to the action
        of closing the top window.

        >>> import pygame as pg
        >>> from pygwin.all import WindowSystem
        >>> _ = pg.init()
        >>> screen = pg.display.set_mode((800, 600))
        >>> win_sys = WindowSystem(screen)
        >>> win_sys.bind_key(pg.K_RETURN, 'activate')
        >>> def print_help():
        ...    print('typing F1 prints this help message')
        >>> win_sys.bind_key(pg.K_F1, 'user-defined', fun=print_help)
        >>> win_sys.bind_key(pg.K_F1, 'close-window', pressed=[pg.K_LCTRL])

        """
        if action is not None and action not in types.all_key_actions:
            logging.warning('undefined action: %s', action)
            return
        p: tp.Tuple[int, ...]
        if pressed is None:
            p = tuple()
        else:
            p = tuple(sorted(pressed))
        if action is None:
            if key in self._key_map:
                if p in self._key_map[key]:
                    del self._key_map[key][p]
                if self._key_map[key] == dict():
                    del self._key_map[key]
        else:
            if key not in self._key_map:
                self._key_map[key] = dict()
            self._key_map[key][p] = action, fun
