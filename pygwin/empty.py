#!/usr/bin/env python3

"""Definition of class Empty."""

from .node import Node


class Empty(Node):
    """Empty nodes are ... empty nodes.

    Empty nodes can be used as placeholders for a background image or
    to occupy a specific size in the page (using the size style).

    """
