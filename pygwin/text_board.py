#!/usr/bin/env python3

"""Definition of class TextBoard."""

import typing as tp

from . import types, util
from .box import Box
from .image import Image


class TextBoard(Box):
    """TextBoard nodes are boxes that contains text.

    The text in a TextBoard is always split in several Label nodes
    such that each of these Labels does not exceed the width of the
    board.  A TextBoard can grow dynamically with new text using the
    push_text method.

    """

    AVAILABLE_STYLES = {
        'color',
        'text-board-push-dest',
        'text-board-rows'
    }

    text: tp.List[str]

    def __init__(self, text: tp.Optional[str] = None, **kwargs: tp.Any):
        """Initialise a TextBoard containing optional string text."""
        Box.__init__(self, **kwargs)
        self.text = list()
        self._pending: tp.List[str] = list()
        self._text_width: tp.Optional[int] = None
        if text is not None:
            self.push_text(text)

    def push_text(self, text: str) -> None:
        """Push a new text string on the board."""
        self.text.append(text)
        self._pending.append(text)
        self._reset_size()

    def _compute_inner_size(self) -> types.pos_t:
        width = self.get_width()
        if width is None:
            return 0, 0
        if self._text_width != width:
            self._text_width = width
            self.empty()
            text_list = self.text
        elif self._pending != []:
            text_list = self._pending
        else:
            text_list = []
        self._pending = []

        for text in text_list:
            lines = list(
                util.split_lines(
                    text, self.get_font(), self.get_style('color'),
                    width=self._text_width
                )
            )
            dest = self.get_style('text-board-push-dest')
            if dest == 'top':
                lines.reverse()
            for line in lines:
                lbl = Image(line)
                if dest == 'top':
                    self.insert(0, lbl)
                else:
                    self.pack(lbl)

        self._remove_overflowing_lines()
        return Box._compute_inner_size(self)

    def _remove_overflowing_lines(self) -> None:
        rows = self.get_style('text-board-rows')
        if rows is not None:
            dest = self.get_style('text-board-push-dest')
            while len(self.children) > rows:
                if dest == 'top':
                    self.remove(rows)
                else:
                    self.remove(0)
