#!/usr/bin/env python3

"""Definition of class Label."""

import typing as tp
import functools
import pygame as pg

from . import types, util
from .node import Node


class Label(Node):
    """Label nodes are used to draw texts."""

    AVAILABLE_STYLES = {
        'color',
        'underline'
    }

    def __init__(
            self,
            text: str,
            **kwargs: tp.Any
    ) -> None:
        """Initialise a label with the given text.

        If not None, kwarg label_for must be a Node object.  If the
        label is clicked or activated then node label_for is activated.

        """
        Node.__init__(self, **kwargs)
        self._text: str
        label_for: tp.Optional[Node] = kwargs.get('label_for')
        if label_for is not None:
            def link() -> bool:
                assert label_for is not None
                label_for.activate()
                return True
            self.set_link(link)
        self.text = text

    @property
    def text(self) -> str:
        """Text of the label."""
        return self._text

    @text.setter
    def text(self, text: str) -> None:
        """Set the text of the label."""
        self._text = text
        self._reset_size()

    @classmethod
    def node_of(cls, value: tp.Any, **kwargs: tp.Any) -> Node:
        """Return value if it is a Node, or Label(value, **kwargs) else."""
        if isinstance(value, Node):
            result = value
        else:
            result = Label(str(value), **kwargs)
        return result

    @classmethod
    @functools.lru_cache(maxsize=10000)
    def _render_cache(
            cls,
            text: str,
            font: pg.font.Font,
            color: types.color_t,
            **kwargs: tp.Any
    ) -> pg.surface.Surface:
        font.set_underline(kwargs.get('underline', False))
        result = next(util.split_lines(text, font, color))
        font.set_underline(False)
        return result

    @classmethod
    def _render(
            cls,
            text: str,
            font: pg.font.Font,
            color: tp.Any,
            **kwargs: tp.Any
    ) -> pg.surface.Surface:
        return Label._render_cache(text, font, tuple(color), **kwargs)

    def _redraw(self) -> pg.surface.Surface:
        return Label._render(
            self._text,
            self.get_font(),
            self.get_style('color'),
            underline=self.get_style('underline')
        )

    def _compute_inner_size(self) -> types.pos_t:
        return self._redraw().get_size()

    def _draw(self, surface: pg.surface.Surface, pos: types.pos_t) -> None:
        surface.blit(self._redraw(), pos)
