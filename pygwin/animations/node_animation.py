#!/usr/bin/env python3

"""Definition of class NodeAnimation."""

import abc
import typing as tp

from ..animation import Animation
if tp.TYPE_CHECKING:
    from ..node import Node  # pylint: disable=unused-import


class NodeAnimation(Animation, abc.ABC):
    """A NodeAnimation periodically update the style of a Node."""

    node: 'Node'
    tmp_class: tp.Optional[str]

    def __init__(
            self,
            node: 'Node',
            prog: tp.Any,
            handler: tp.Callable[[tp.Any], tp.Any],
            **kwargs: tp.Any
    ):
        """Initialize self.

        Argument node is the Node object on which self will operate.

        kwarg period is the period of the animation (default = 1).

        If kwarg persistent is False then the node recovers its
        original style when the animation ends.  Otherwise, the
        updates of the style made by the animation are permanent.
        Default is False.

        The animation is started after initialisation.

        """
        Animation.__init__(
            self,
            prog,
            handler,
            node.get_window(),
            period=kwargs.get('period', 1)
        )
        self.node = node
        if kwargs.get('persistent', False):
            self.tmp_class = None
        else:
            self.tmp_class = self.node._new_tmp_style_class()
        node.set_animation(self)

    def stop(self) -> None:
        super().stop()
        if self.node.animation == self:
            self.node.set_animation(None)
        if self.tmp_class is not None:
            self.node._del_tmp_style_class(self.tmp_class)
            self.tmp_class = None
