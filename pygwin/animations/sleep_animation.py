#!/usr/bin/env python3

"""Definition of class SleepAnimation."""

import typing as tp
import pygame as pg

from ..all import Animation
if tp.TYPE_CHECKING:
    from ..all import Window  # pylint: disable=unused-import


class SleepAnimation(Animation):
    """A SleepAnimation only waits some time and terminates."""

    def __init__(self, win: 'Window', sleep_time: int):
        def sleep(_: bool) -> tp.Optional[bool]:
            if pg.time.get_ticks() - start_time <= sleep_time:
                return True
            return None
        start_time = pg.time.get_ticks()
        super().__init__(True, sleep, win, period=1)
