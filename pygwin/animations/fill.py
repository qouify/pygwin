#!/usr/bin/env python3

"""Definition of class FillAnimation."""

import typing as tp

from .node_animation import NodeAnimation
if tp.TYPE_CHECKING:
    from ..all import ValuedNode  # pylint: disable=unused-import
    from ..min_max import _MinMax  # pylint: disable=unused-import

    class _MinMaxNode(ValuedNode[int], _MinMax):
        pass


class FillAnimation(NodeAnimation):
    """A FillAnimation increases/decreases a Range or Gauge value."""

    def __init__(self, node: '_MinMaxNode', **kwargs: tp.Any):
        """Initialise a FillAnimation for node.

        kwarg step (default = 1) specifies how the value of the node
        is increased.

        """
        def handler(_: bool) -> tp.Optional[bool]:
            value = node.value + step
            value = min(node.max_value, max(node.min_value, value))
            result: tp.Optional[bool] = True
            if not node.min_value < value < node.max_value:
                result = None
            node.set_value(value)
            return result
        step = kwargs.get('step', 1)
        NodeAnimation.__init__(self, node, True, handler, **kwargs)
