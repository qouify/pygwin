#!/usr/bin/env python3

"""Definition of various functions for cursor management."""

import logging
import typing as tp
import pygame as pg

from . import media


status_t = tp.Literal['base', 'clicked']  # pylint: disable=invalid-name

_ON: bool = False  # pylint: disable=invalid-name
_DEFAULT: tp.Dict[  # pylint: disable=invalid-name
    status_t,
    pg.surface.Surface
] = dict()


def set_default(
        img: tp.Union[str, pg.surface.Surface],
        status: status_t = 'base'
) -> None:
    """Set the default image file of status.

    img can be either the file path of the image or either a pygame
    Surface.

    """
    surf = media.get_image(img)
    if surf is not None:
        _DEFAULT[status] = surf


def get_default(
        status: status_t = 'base'
) -> tp.Optional[pg.surface.Surface]:
    """Get the default image of status."""
    return _DEFAULT.get(status)


def activate() -> None:
    """Activate pygwin's cursor system.

    The cursor image of the base context must have been set before
    this to work. If successful, the system cursor becomes invisible.

    """
    global _ON  # pylint: disable=global-statement
    if 'base' not in _DEFAULT:
        logging.error('default cursor image is not set')
    else:
        _ON = True
        pg.mouse.set_visible(False)


def deactivate() -> None:
    """Deactivate pygwin's cursor system.

    The system cursor becomes visible again.

    """
    global _ON  # pylint: disable=global-statement
    _ON = False
    pg.mouse.set_visible(True)


def activated() -> bool:
    """Check if the cursor system has been activated."""
    return _ON
