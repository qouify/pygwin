#!/usr/bin/env python3

"""Definition of class Radiobox."""

import typing as tp

from .checkbox import Checkbox
if tp.TYPE_CHECKING:
    from .radiobox_group import RadioboxGroup  # pylint: disable=unused-import


class Radiobox(Checkbox):
    """Radiobox nodes are similar to HTML <input type="radio">."""

    def __init__(
            self,
            group: 'RadioboxGroup',
            group_value: tp.Any,
            **kwargs: tp.Any
    ):
        """Initialise a radiobox put in group and associated with group_value.

        kwarg group_value is the value that will be associated to this
        radiobox in the group.  This means that if the radiobox is
        checked, then group.value will return group_value.

        kwarg value is True if the radiobox is initially checked.

        >>> from pygwin.all import RadioboxGroup
        >>> grp_grade = RadioboxGroup()
        >>> box_a = Radiobox(grp_grade, 'A')
        >>> box_b = Radiobox(grp_grade, 'B', value=True)
        >>> box_c = Radiobox(grp_grade, 'C')
        >>> box_d = Radiobox(grp_grade, 'D')
        >>> box_e = Radiobox(grp_grade, 'E')
        >>> grp_grade.get_value()
        'B'
        >>> box_b.value
        True
        >>> _ = box_e.set_value(True)
        >>> grp_grade.get_value()
        'E'
        >>> box_b.value
        False

        """
        Checkbox.__init__(self, **kwargs)
        self._group = group
        self._group.add_radiobox(self, group_value)
        if self.value:
            self._group.select(self)

    def set_value(self, value: tp.Any, trigger: bool = True) -> None:
        super().set_value(value, trigger=trigger)
        if self.value:
            self._group.select(self)

    def _activate(self) -> bool:
        self.get_focus()
        if not self.value:
            self.set_value(True)
        return True
