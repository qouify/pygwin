#!/usr/bin/env python3

"""Definition of class NodeType."""

import typing as tp

from .style_class import StyleClass


class NodeType(type):
    """A NodeType class is any class that has the Node class as ancestor."""

    def __init__(cls, name: tp.Any, bases: tp.Any, dic: tp.Any):
        """Initialise NodeType class cls.

        A new style class is created for cls of which the name is
        cls.__name__.

        """
        type.__init__(cls, name, bases, dic)
        StyleClass(cls.__name__)
