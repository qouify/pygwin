Many thanks to the authors of the media used by the pygwin module:

* team of dungeon crawl stop soup for their monster images
https://crawl.develz.org/

* Lamoot for its RPG GUI contruction kit
https://opengameart.org/content/rpg-gui-construction-kit-v10

* Kenney for its RPG GUI contruction kit
https://www.kenney.nl

* rhodesmas (for the 'click on checkbox' sound)
https://freesound.org/people/rhodesmas/sounds/380291/

* gamedevc (for the 'over link' sound)
https://freesound.org/people/GameDevC/sounds/422830/

* complex_waveform (for the 'key' sound)
https://freesound.org/people/complex_waveform/sounds/213148/

* Daniel Lyons (for the 'ltinternet' font)
https://www.dafont.com/lt-internet.font

* FontBlast Design (for the 'monaco' font)
https://www.dafont.com/monaco.font

* Dieter Steffmann (for the 'cardinal' font)
https://www.1001freefonts.com/cardinal.font

* James Grieshaber (for the 'metamorphous' font)
https://www.1001freefonts.com/metamorphous.font

* Dharma Type (for the 'bebas-neue' font)
https://www.dafont.com/fr/bebas-neue.font
