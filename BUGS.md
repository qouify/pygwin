* Gauge nodes with rounded corners are not drawn correctly.

* A relative size style in a floating node raises an AssertionError.