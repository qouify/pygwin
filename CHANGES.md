* Package `keys` has been removed.  `WindowSystem.bind_key` should not
  be used in replacement of `Keys.bind`.

* `AnimationSequence` can now be initialised with an empty sequence.
  (It previously crashed if initialised with e.g. `[]`.)

* Changed the signature of `Window.__init__`: replaced argument `node:
  Node` by `*nodes: Node`.

* Changed the signature of `Box.pack`: `pack(self, node: Node)` ->
  `pack(self, *nodes: Node)`.

* Changed the signature of `WindowSystem.process_pg_event`:
  `process_pg_event(self, pgevt: pg.event.Event)` ->
  `process_pg_event(self, *pgevts: pg.event.Event)`.

* Typing: changed `typing.Dict` to `typing.Mapping` in `Table`.

* Typing: class `ValuedNode` has been made generic (with respect to
  the type of value carried by the node).
