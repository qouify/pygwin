#!/usr/bin/env python3

"""A simple example game to illustrate the use of pygwin.

The game simply consists of moving a square with arrow keys.  The game
is lost if the square goes outside the screen.  Pressing the escape
key opens a menu window.

"""

#  import pygame and pygwin
import pygame as pg
import pygwin as pw

#  usual pygame initialisation code
pg.init()
screen = pg.display.set_mode((800, 600))
clock = pg.time.Clock()

#  initialise a pygame window system attached to our main surface
win_sys = pw.WindowSystem(screen)

#  load default style classes included in the pygwin package.  this
#  defines default colors, borders, font, ...
pw.StyleClass.load_default()

#  game constants
SIZE = (10, 10)  # size of the square
SPEED = 10  # speed of the square
COLOR = (255, 0, 0)  # color of the square
INIT_POS = (100, 100)  # initial position of the square
INIT_DIRECTION = (0, SPEED)  # initial direction of the square (to the south)

#  game variables
pos = INIT_POS  # position of the square
direction = INIT_DIRECTION  # direction of the square
game_over = False  # is the game over?


def reset_game():
    """Reset game related global variables to start a new game."""
    global pos, direction, game_over
    pos = INIT_POS
    direction = INIT_DIRECTION
    game_over = False


def open_menu_window():
    """Function that opens the menu window."""

    def activate_new():  # called when the new game button is clicked
        reset_game()
        win.close()
        return True  # return True to notify pygwin that clicking the
                     # button updated the interface
    btn_new = pw.Button(
        pw.Label('New game'),  # what's inside the button
        link=activate_new  # clicking on the button calls activate_new
    )

    def activate_quit():  # called when the quit button is clicked
        global loop
        loop = False
        win.close()
        return True
    btn_quit = pw.Button(pw.Label('Quit game'), link=activate_quit)

    def activate_close():  # called when the close button is clicked
        win.close()
        return True
    btn_close = pw.Button(pw.Label('Close menu'), link=activate_close)

    win = pw.Window(
        win_sys,     # the window is attached to our WindowSystem.
        btn_new,     # it contains button btn_new,
        btn_quit,    # button btn_quit, and
        btn_close,   # button btn_close.
        title='Menu'
    )
    win.open()


def open_game_over_window():
    """Function that opens the game over window."""
    lbl = pw.Label('close the window to start a new game')
    win = pw.Window(win_sys, lbl, title='Game Over!')
    win.open()


def escape_key():
    """Function that will be called when the user presses the escape key."""
    #  if a window is already opened, we close it.  otherwise we open
    #  the menu window
    top_win = win_sys.top_window()
    if top_win is None:
        open_menu_window()
    else:
        top_win.close()
    return True


#  we bind the escape key to the escape_key function we just defined
win_sys.bind_key(pg.K_ESCAPE, 'user-defined', fun=escape_key)

#  main game loop
loop = True
while loop:

    #  save pygame events in evts and have the window system process
    #  them
    evts = [evt for evt in pg.event.get()]
    win_sys.process_pg_event(*evts)

    if win_sys.top_window() is None:  # no window opened => the game is running

        #  update the direction if the user presses an arrow key
        for evt in evts:
            if evt.type == pg.KEYDOWN:
                try:
                    direction = {
                        pg.K_DOWN: (0, SPEED),
                        pg.K_LEFT: (- SPEED, 0),
                        pg.K_RIGHT: (SPEED, 0),
                        pg.K_UP: (0, -SPEED)
                    }[evt.key]
                except KeyError:  # not an arrow key => we ignore it
                    pass

        #  update the position and check if we're still in the screen
        pos = pos[0] + direction[0], pos[1] + direction[1]
        rect = pg.Rect(pos, SIZE)
        screen_rect = pg.Rect((0, 0), screen.get_size())
        game_over = not screen_rect.contains(rect)

    screen.fill((0, 0, 0, 0))

    #  if game is not over we draw the rectangle else open the game
    #  over window and reset the game variables
    if not game_over:
        pg.draw.rect(screen, COLOR, rect)
    else:
        open_game_over_window()
        reset_game()

    #  refresh the window system (=> redraw it) and update the display
    win_sys.refresh()
    pg.display.update()
    clock.tick(40)

pg.quit()
