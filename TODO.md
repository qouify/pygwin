# missing features to implement

* allow regular expressions in style input-text-allowed

* horizontal scoll bars in frames

* rowspan in tables

* possibility to have style classes inherit from other style classes


#  stuffs that should be coded

* merge Node.iter_tree and EventManager.iter_nodes

* node.py is growing too large.  it should be split in several files

* needs a clever lazy redrawing mode

* factorise code in Node.__draw_border_images and Rule._draw

* use lru cache of functools for InputText.__redraw

* WindowSystem should maybe inherit from event_manager
