* `animation`
  * description: type of animation triggered
  * accepted values: `typing.Literal['fade', 'fadein', 'fadeout', 'fill', 'grow', 'glow', 'popin', 'popout', 'scroll']`
  * available for: `Node`
* `animation-arguments`
  * description: arguments of the animation (valid only if `animation` is not `None`)
  * accepted values: `typing.Dict[str, typing.Any]`
  * available for: `Node`
* `background`
  * description: type of background for the node
  * accepted values: `typing.Literal['color', 'image']`
  * available for: `Node`
* `background-color`
  * description: background color of the node (valid only if `background` == `'color'`)
  * accepted values: `typing.Union[str, typing.Tuple[int, int, int], typing.Tuple[int, int, int, int]]`
  * available for: `Node`
* `background-image`
  * description: file path of the background image (valid only if `background` == `'image'`)
  * accepted values: `<class 'str'>`
  * available for: `Node`
* `border`
  * description: type of the border for the node
  * accepted values: `typing.Literal['color', 'image']`
  * available for: `Node`
* `border-color`
  * description: border color of the node (valid only if `border` == `'color'`)
  * accepted values: `typing.Union[str, typing.Tuple[int, int, int], typing.Tuple[int, int, int, int]]`
  * available for: `Node`
* `border-images`
  * description: Document this.
  * accepted values: `typing.Tuple[str, str, str, str, str, str]`
  * available for: `Node`
* `border-width`
  * description: width (in pixels) of the border of the node (valid only if
`border` == `'color'`)
  * accepted values: `<class 'int'>`
  * available for: `Node`
* `color`
  * description: color of the node
  * accepted values: `typing.Union[str, typing.Tuple[int, int, int], typing.Tuple[int, int, int, int]]`
  * available for: `Checkbox`, `Gauge`, `InputText`, `Label`, `TextBoard`
* `corner`
  * description: if positive, the border of the node is a rounded rectangle and its
corners have the specified value as radius
  * accepted values: `<class 'int'>`
  * available for: `Node`
* `cursor-image`
  * description: file path of the cursor image
  * accepted values: `<class 'str'>`
  * available for: `Node`
* `expand`
  * description: valid only if the node is a child of a Box node.  if `True` the node's
container size is expanded to occupy all the remaining space in the
box it belongs to
  * accepted values: `<class 'bool'>`
  * available for: `Node`
* `font`
  * description: file path of the font
  * accepted values: `<class 'str'>`
  * available for: `Node`
* `font-size`
  * description: font size of the node
  * accepted values: `typing.Union[int, typing.Literal['xx-small', 'x-small', 'small', 'normal', 'large', 'x-large', 'xx-large']]`
  * available for: `Node`
* `frame-bar-background-color`
  * description: background color of the frame scroll bars
  * accepted values: `typing.Union[str, typing.Tuple[int, int, int], typing.Tuple[int, int, int, int]]`
  * available for: `Frame`
* `frame-bar-color`
  * description: color of the frame scroll bars
  * accepted values: `typing.Union[str, typing.Tuple[int, int, int], typing.Tuple[int, int, int, int]]`
  * available for: `Frame`
* `frame-bar-corner`
  * description: radius in pixels of the rounded corners of the frame scrollbars
  * accepted values: `<class 'int'>`
  * available for: `Frame`
* `frame-bar-width`
  * description: width in pixels of the frame scroll bars
  * accepted values: `<class 'int'>`
  * available for: `Frame`
* `frame-vbar-images`
  * description: Document this.
  * accepted values: `typing.Tuple[str, str, str, str, str, str]`
  * available for: `Frame`
* `gauge-label-class`
  * description: style class of the label of the gauge
  * accepted values: `<class 'str'>`
  * available for: `Gauge`
* `gauge-label-format`
  * description: a format string used for the label of the gauge.  if `None`, the gauge
does not have a label.  formatting variables are `{min}`, `{value}`,
and `{max}` standing for the minimal, current and maximal values of
the gauge respectively
  * accepted values: `<class 'str'>`
  * available for: `Gauge`
* `grid-row-size`
  * description: maximal number of children nodes in a row of the grid
  * accepted values: `<class 'int'>`
  * available for: `Grid`
* `halign`
  * description: horizontal aligment of the node
  * accepted values: `typing.Literal['center', 'left', 'right']`
  * available for: `Node`
* `hspacing`
  * description: horizontal space between two children nodes
  * accepted values: `<class 'int'>`
  * available for: `Box`, `Table`
* `input-text-allowed`
  * description: list of allowed characters in the input text.  meta expressions of
regular expressions are accepted
  * accepted values: `<class 'str'>`
  * example values:
    * `\dA-Fa-f`
    * `\w `
  * available for: `InputText`
* `input-text-max-size`
  * description: maximal number of characters that may be typed in the input text
  * accepted values: `<class 'int'>`
  * available for: `InputText`
* `input-text-placeholder`
  * description: string displayed in the input text if its value is `''`.  `None` or
`''` disables the place holder string
  * accepted values: `<class 'str'>`
  * available for: `InputText`
* `opacity`
  * description: Document this.
  * accepted values: `<class 'float'>`
  * available for: `Node`
* `orientation`
  * description: specify how children nodes are placed within the Box
  * accepted values: `typing.Literal['vertical', 'horizontal']`
  * available for: `Box`
* `padding`
  * description: space (in pixels) between the node and its border
  * accepted values: `typing.Union[int, typing.Tuple[int, int]]`
  * available for: `Node`
* `pos`
  * description: valid only for floating nodes, tooltips or contextual menus.  sets the
position of the node
  * accepted values: `typing.Tuple[typing.Literal['relative', 'absolute'], typing.Tuple[typing.Literal['left', 'center', 'right'], typing.Literal['top', 'center', 'bottom']], typing.Tuple[int, int]]`
  * example values:
    * `('absolute', ('right', 'bottom'), (0, 0))`
    * `('relative', ('right', 'top'), (10, 10))`
  * available for: `Node`
* `pos-list`
  * description: valid only for tooltips or contextual menus.  the node will be placed
at the first position of the list that will make it fully visible in
the window.  if there is no such position, the first position of the
list is used
  * accepted values: `typing.List[typing.Tuple[typing.Literal['relative', 'absolute'], typing.Tuple[typing.Literal['left', 'center', 'right'], typing.Literal['top', 'center', 'bottom']], typing.Tuple[int, int]]]`
  * available for: `Node`
* `range-acceleration`
  * description: acceleration of the range update speed when the user keeps an arrow key pressed
on the range
  * accepted values: `<class 'int'>`
  * available for: `Range`
* `range-bar-color`
  * description: color of the horizontal bar of the range
  * accepted values: `typing.Union[str, typing.Tuple[int, int, int], typing.Tuple[int, int, int, int]]`
  * available for: `Range`
* `range-bar-corner`
  * description: radius of the rounded corners of the range horizontal bar
  * accepted values: `<class 'int'>`
  * available for: `Range`
* `range-bar-size`
  * description: size (width + height) of the range horizontal bar
  * accepted values: `typing.Tuple[int, int]`
  * available for: `Range`
* `range-bullet-color`
  * description: color of the range bullet
  * accepted values: `typing.Union[str, typing.Tuple[int, int, int], typing.Tuple[int, int, int, int]]`
  * available for: `Range`
* `range-bullet-radius`
  * description: radius of the range bullet
  * accepted values: `<class 'int'>`
  * available for: `Range`
* `range-label-class`
  * description: style class of the range label.  `None` if no style
  * accepted values: `<class 'str'>`
  * available for: `Range`
* `range-label-distance`
  * description: distance (in pixels) of the range label from the horizontal bar
  * accepted values: `<class 'int'>`
  * available for: `Range`
* `range-label-format`
  * description: formatting string for the range label.  `None` disables the label.
see style `gauge-label-format` for the different formatting variables
  * accepted values: `<class 'str'>`
  * available for: `Range`
* `range-step`
  * description: specifies how much a Range is modified by a user input
  * accepted values: `<class 'int'>`
  * available for: `Range`
* `rule-images`
  * description: Document this.
  * accepted values: `typing.Tuple[str, str, str]`
  * available for: `Rule`
* `scale`
  * description: Document this.
  * accepted values: `<class 'float'>`
  * available for: `Node`
* `select-cyclic`
  * description: specify if the select is cyclic
  * accepted values: `<class 'bool'>`
  * available for: `Select`
* `select-hide-links`
  * description: specifiy if navigation links become hidden when disabled
  * accepted values: `<class 'bool'>`
  * available for: `Select`
* `select-next-class`
  * description: style class of the move-to-next navigation link
  * accepted values: `<class 'str'>`
  * available for: `Select`
* `select-next-label`
  * description: text of the move-to-next navigation link
  * accepted values: `<class 'str'>`
  * available for: `Select`
* `select-prev-class`
  * description: style class of the move-to-previous navigation link
  * accepted values: `<class 'str'>`
  * available for: `Select`
* `select-prev-label`
  * description: text of the move-to-previous navigation link
  * accepted values: `<class 'str'>`
  * available for: `Select`
* `select-wheel-units`
  * description: number of items skipped when the mouse wheel is used over the Select
  * accepted values: `<class 'int'>`
  * available for: `Select`
* `size`
  * description: size of the node as a tuple `(width, height)`
  * accepted values: `typing.Tuple[typing.Union[str, int, NoneType], typing.Union[str, int, NoneType]]`
  * example values:
    * `(100, 100)`
    * `('100%', None)`
    * `(None, '100%')`
    * `(400, '100%')`
  * available for: `Node`
* `sound`
  * description: path of the sound file played
  * accepted values: `<class 'str'>`
  * available for: `Node`
* `text-board-push-dest`
  * description: specify where new text strings are pushed in the TextBoard
  * accepted values: `typing.Literal['bottom', 'top']`
  * available for: `TextBoard`
* `text-board-rows`
  * description: maximal number of rows in the TextBoard
  * accepted values: `<class 'int'>`
  * available for: `TextBoard`
* `underline`
  * description: specify if the text label is underlined or not
  * accepted values: `<class 'bool'>`
  * available for: `Label`
* `valign`
  * description: vertical alignment of the node
  * accepted values: `typing.Literal['bottom', 'center', 'top']`
  * available for: `Node`
* `vspacing`
  * description: vertical space between two children nodes
  * accepted values: `<class 'int'>`
  * available for: `Box`, `Table`
* `window-cross-image`
  * description: file path of the window's cross image
  * accepted values: `<class 'str'>`
  * available for: `Window`
