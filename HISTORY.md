#  Version 0.3.0

* new animation: `FillAnimation` for `Gauge` or `Range` nodes

* new animation: `SleepAnimation` that just sleeps for some time

* new animation: `AnimationSequence` that can be used to chain
  animations

* new style: `range-step` to specify how much a `Range` is modified by
  a user input

* removed `pygwin.default_style`.  `DefaultStyle.load` has been
  replaced by `StyleClass.load_default`

* the period of an `Animation` object is now expressed in iterations
  of the main loop rather than ms which made it inacurate.

* updated the default value of the `start_now` argument of
  `Animation.start` (from `True` to `False`)

* bug fix: animations of the top window did not run if several windows
  were opened

* bug fix: `Table.empty` did not call `_del_child` on children nodes

* bug fix: `Box.empty` did not call `_del_child` on children nodes

* bug fix: `Animation.start` had no effect if the animation was paused


#  Version 0.2.0

* new node type: `Range` (similar to HTML's `<input type="range"/>`)

* implementation of various animations: grow, glow, fade, scroll (see
  animations window in the test application)

* many new styles have been added and many kwarg argument of node
  constructors are now styles.  see `STYLES.md` to see all styles

* modified the way styles are defined (see `data/default-style.json`).
  before:
  ```json
  {
  "Button": {
        "base": {"corner": 6},
        "overed": {"color-background": [0, 0, 128]},
        "clicked": {"color-background": [0, 50, 100]}
  }
  }
  ```
  now:
  ```json
  {
  "Button": {"corner": 6},
  "Button:status=overed": {"color-background": [0, 0, 128]},
  "Button:status=clicked": {"color-background": [0, 50, 100]}
  }
  ```

* styles can now depend on values of nodes:
  ```python
    cb = Checkbox()
    cb.set_style(
        {'color-border': (0, 255, 0)},
        context={'value': True}
    )
  ```
  corresponding json definition:
  ```json
  { "Checkbox:value=True": { "color-border", [0, 255, 0] } }
  ```

* styles can now depend on the class of the parent node.  for example,
  to say that all nodes inside tables are vertically centered:
  ```json
  {"Node:parentclass=Table": {"valign": "center"}}
  ```

* possibility to load fonts from json file (see `pygwin/data/fonts.json`)

* `font-size` attributes can now also be strings (`xx-small`,
  `x-small`, `small`, `normal`, `large`, `x-large`, `xx-large`)

* modified the positioning system used for tooltips, contextual menus
  and floating nodes.  a candidate position to put a such a node was
  of the form `('relative', ('left', 0), ('top', 100))`.  now it's
  `('relative', ('left', 'top'), (0, 100))`

* removed obsolete style `image-checkbox`.  we now use
  `image-background` with value dependent styles

* removed obsolete style `image-select`.  navigation links of Select
  nodes can now be configured with styles `select-next-class`,
  `select-next-label`, `select-prev-class` and `select-prev-label`

* added typing informations in all files (except in `pygwin.test`
  module)

* split `pygwin/test/test.py` in several files

* renamed `cls` by `stc` (style classes) in `node.py`

* when creating an `InputText` with a non empty initial value, the
  cursor is placed at the end of the text

* the `EventManager.add_floating_node` method no longer accepts a
  `pos` argument.  use style `pos` instead

* the `EventManager.set_floating_node_pos` method has been removed.
  use style `pos` instead

* the `Node.set_tooltip` and `Node.set_ctx_menu` no longer accept a
  `candidate_pos` argument.  use style `pos` or `pos-list` instead

* any animation is now linked to a `Window`.  this means that when the
  window is closed, all its animations are stopped

* split `Rule` in `HorizontalRule` and `VerticalRule`.  style
  `orientation` is thus not used anymore for `Rule` nodes


#  Version 0.1.0

* new node type: `Grid` (updated the test program accordingly)

* new style: `corner` to have nodes with rounded corners

* new style: `frame-bar-corner` to have frame bars with rounded corners

* it is now possible to create style of the form `name:class`.  the
  style only applies to item having style-class name and that are
  object of class, e.g., `link:Label`.  see e.g.,
  `data/default-style.json`

* added `StyleClass.load` to load style classes from a json file

* added default style link for all nodes that are initiated with
  `link=fun` kwarg

* removed deprecated `WindowSystem.__frozen`, `WindowSystem.freeze`
  and `WindowSystem.unfreeze`

* removed useless redrawings in `WindowSystem.loop`

* `Frame` nodes are now focusable

* added `title_node` attribute to `Window` objects

* renamed bubble to tooltip everywhere

* default styles are now stored in `data/default-style.json` rather
  than being encoded in `DefaultStyle`

* label of `Gauge` nodes are now Label nodes (instead of `pygame.Surface`)

* label links of `Select` nodes can be more easily configured

* disabled nodes cannot get the focus anymore

* bug fix: a frame scroll bar can now be clicked when the frame has a
  border and/or padding

* changed the signature of `Box.insert(self, i, node)` instead of
  `Box.insert(self, node, i)` to be consistent with `Box.replace`

* added `__len__`, `__iter__` and `__getitem__` to `Box` nodes


# Version 0.0.1

  * bug fix: label rendering failed if the label was empty
